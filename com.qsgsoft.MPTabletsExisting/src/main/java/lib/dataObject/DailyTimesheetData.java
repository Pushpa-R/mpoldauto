package lib.dataObject;

import lib.dataObject.DailyTimesheetData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class DailyTimesheetData {
    public DailyTimesheetData() throws Exception {
		
	}    
    ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "DailyTimesheetXml";
	
	public final String jobLocationNW1 = xml.read(ConsumerTag.jobLocationNW1, FILEPATH),
			                      jobLocationNW2 = xml.read(ConsumerTag.jobLocationNW2, FILEPATH),
			                    	 jobNumber = xml.read(ConsumerTag.jobNumber, FILEPATH),
			                    	 jobNumberMP = xml.read(ConsumerTag.jobNumberMP, FILEPATH),
			                    	 remarksNW = xml.read(ConsumerTag.remarksNW, FILEPATH),
			                    	 remarksMP = xml.read(ConsumerTag.remarksMP, FILEPATH),
			                    	 woNW1 =  xml.read(ConsumerTag.woNW1, FILEPATH),
			                    	 woNW2 =  xml.read(ConsumerTag.woNW2, FILEPATH),
			                    	 woMP1 =  xml.read(ConsumerTag.woMP1, FILEPATH),
			                    	 woMP2 =  xml.read(ConsumerTag.woMP2, FILEPATH),			                    	 
			                    	 jobLocationMP1 =  xml.read(ConsumerTag.jobLocationMP1, FILEPATH),
			                      jobLocationMP2 =  xml.read(ConsumerTag.jobLocationMP2, FILEPATH),
			                    	 empNameNW1 =  xml.read(ConsumerTag.empNameNW1, FILEPATH),
			                    	 empNameNW2 =  xml.read(ConsumerTag.empNameNW2, FILEPATH),		
			                    	 empNameMP = xml.read(ConsumerTag.empNameMP, FILEPATH),	
			                    	 empNameMP2 = xml.read(ConsumerTag.empNameMP2, FILEPATH),	
			                      jobLocationText = xml.read(ConsumerTag.jobLocationText, FILEPATH),
			                    	 empNamefirst = xml.read(ConsumerTag.empNamefirst, FILEPATH),
			                    	 stNW1 = xml.read(ConsumerTag.stNW1, FILEPATH),
			                    	 stNW2 = xml.read(ConsumerTag.stNW2, FILEPATH),
			                    	 stMP1 = xml.read(ConsumerTag.stMP1, FILEPATH),
			                    	 stMP2 = xml.read(ConsumerTag.stMP2, FILEPATH), 
			                    	 otNW1 = xml.read(ConsumerTag.otNW1, FILEPATH), 
			                    	 otNW2 = xml.read(ConsumerTag.otNW2, FILEPATH), 
			                    	 otMP1 = xml.read(ConsumerTag.otMP1, FILEPATH), 
			                    	 otMP2 = xml.read(ConsumerTag.otMP2, FILEPATH),  
			                    	 dtNW1 = xml.read(ConsumerTag.dtNW1, FILEPATH),  
			                    	 dtNW2 = xml.read(ConsumerTag.dtNW2, FILEPATH),  
			                    	 dtMP1 = xml.read(ConsumerTag.dtMP1, FILEPATH),  
			                    	 dtMP2 = xml.read(ConsumerTag.dtMP2, FILEPATH),
			                    	 perdiemNW1 = xml.read(ConsumerTag.perdiemNW1, FILEPATH),
			                    	 perdiemNW2 = xml.read(ConsumerTag.perdiemNW2, FILEPATH),
			                    	 perdiemMP1 = xml.read(ConsumerTag.perdiemMP1, FILEPATH),
			                    	 perdiemMP2 = xml.read(ConsumerTag.perdiemMP2, FILEPATH),
			                    	 equipHours = xml.read(ConsumerTag.equipHours, FILEPATH),			                    	 
			                      OK = xml.read(ConsumerTag.OK, FILEPATH),
			                    	 Yes = xml.read(ConsumerTag.Yes, FILEPATH),
			                      alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH);
	
	public static class ConsumerTag {
		public static final String jobLocationNW1 = "jobLocationNW1",
				jobLocationNW2 = "jobLocationNW2",
			    jobNumberMP = "jobNumberMP",
				jobNumber = "jobNumber",
			    remarksNW = "remarksNW",
			    	remarksMP = "remarksMP",
				woNW1 = "woNW1",
				woNW2 = "woNW2",
				woMP1 = "woMP1",
				woMP2 = "woMP2",
				stMP1 = "stMP1",
				stMP2 = "stMP2",
				otNW1 = "otNW1",
				otNW2 = "otNW2",
				otMP1 = "otMP1",
				otMP2 = "otMP2",
				dtNW1 = "dtNW1",
				dtNW2 = "dtNW2",
				dtMP1 = "dtMP1",
				dtMP2 = "dtMP2",
				Yes = "Yes",
				perdiemNW1 = "perdiemNW1",
				perdiemNW2 = "perdiemNW2",
				perdiemMP1 = "perdiemMP1",
				perdiemMP2 = "perdiemMP2",
				equipHours = "equipHours",
				jobLocationMP1 = "jobLocationMP1",
				jobLocationMP2 = "jobLocationMP2",
				empNameNW1 = "empNameNW1",
				empNameNW2 = "empNameNW2",
				jobLocationText = "jobLocationText",
				empNamefirst = "empNamefirst",
				empNameMP = "empNameMP",
				empNameMP2 = "empNameMP2",
				stNW1 = "stNW1",
				stNW2 = "stNW2",
				OK = "OK",
				alertForSubmit = "alertForSubmit";
	}
}
