package lib.dataObject;
import java.util.Random;

import lib.dataObject.RoadReadyData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class RoadReadyData {
            public  RoadReadyData() throws Exception {
		
	         }
            ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
        	    static final String FILEPATH = "RoadReadyXml";
        	    
        	    public final String unit = xml.read(ConsumerTag.unit, FILEPATH),
        	    		                             unitsecond = xml.read(ConsumerTag.unitsecond, FILEPATH),
        	    		                             trailer = xml.read(ConsumerTag.trailer, FILEPATH),
        	    		                             fromJob = xml.read(ConsumerTag.fromJob, FILEPATH),
        	    		                             toJob = xml.read(ConsumerTag.toJob, FILEPATH),
        	    		                             employeeName = xml.read(ConsumerTag.employeeName, FILEPATH),
        	    		                             alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH),
        	    		                             OK = xml.read(ConsumerTag.OK, FILEPATH);
        	    
        	    public static class ConsumerTag {
        	    	         public static final String unit = "unit",
        	    	        		                                         unitsecond = "unitsecond",
        	    	        		                                         trailer = "trailer",
        	    	        		                                         fromJob = "fromJob",
        	    	        		                                         toJob = "toJob",
        	    	        		                                         employeeName = "employeeName",
        	    	        		                                         alertForSubmit = "alertForSubmit",
        	    	        		                                         OK = "OK";
        	    }

}
