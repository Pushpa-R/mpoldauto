package lib.dataObject;
import java.util.Random;
import lib.dataObject.IncidentData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;


public class IncidentData {
	public IncidentData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "ReportIncidentXml";
	
	public final String nearmiss = xml.read(ConsumerTag.nearmiss, FILEPATH),
			                         vehicle = xml.read(ConsumerTag.vehicle, FILEPATH),
					                 empName = xml.read(ConsumerTag.empName, FILEPATH),
					                 empNameSecond = xml.read(ConsumerTag.empNameSecond, FILEPATH),
							         dateOfIncident = xml.read(ConsumerTag.dateOfIncident, FILEPATH),
									 dateOfEvaluation = xml.read(ConsumerTag.dateOfEvaluation, FILEPATH),
									 time = xml.read(ConsumerTag.time, FILEPATH),									 
									 jobNumber = xml.read(ConsumerTag.jobNumber, FILEPATH),
									 addressOfIncident = xml.read(ConsumerTag.addressOfIncident, FILEPATH),
									 ExplainIncident = xml.read(ConsumerTag.ExplainIncident, FILEPATH),
									 handTools = xml.read(ConsumerTag.handTools, FILEPATH),
									 PowerTools = xml.read(ConsumerTag.PowerTools, FILEPATH),
								     trainingYes = xml.read(ConsumerTag.trainingYes, FILEPATH),								     
								    	 trainingText = xml.read(ConsumerTag.trainingText, FILEPATH),
								    	 avoidYes = xml.read(ConsumerTag.avoidYes, FILEPATH),
								    	 avoidText = xml.read(ConsumerTag.avoidText, FILEPATH),
								    	 witnessYes = xml.read(ConsumerTag.witnessYes, FILEPATH),
								    	 witnessText = xml.read(ConsumerTag.witnessText, FILEPATH),
								    	 faulty = xml.read(ConsumerTag.faulty, FILEPATH),								    	 
								    	 poor = xml.read(ConsumerTag.poor, FILEPATH),
								    	 explainText = xml.read(ConsumerTag.explainText, FILEPATH),
								     unit = xml.read(ConsumerTag.unit, FILEPATH),
								     ticket = xml.read(ConsumerTag.ticket, FILEPATH),
								    	 attachPhoto = xml.read(ConsumerTag.attachPhoto, FILEPATH),
								    	 cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),								    	 
								     photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
						             photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),								    	 
								    	 photoButton = xml.read(ConsumerTag.photoButton, FILEPATH),
								    	 cameraButton = xml.read(ConsumerTag.cameraButton, FILEPATH),
								     employeeSig = xml.read(ConsumerTag.employeeSig, FILEPATH),
								     employeeName = xml.read(ConsumerTag.employeeName, FILEPATH),
								    	 employeeDate = xml.read(ConsumerTag.employeeDate, FILEPATH),
								    	 saveButton = xml.read(ConsumerTag.saveButton, FILEPATH),								    	 
								    	 firstNext = xml.read(ConsumerTag.firstNext, FILEPATH),								    	 
								    	 secondNext = xml.read(ConsumerTag.secondNext, FILEPATH),
								    	 thirdNext = xml.read(ConsumerTag.thirdNext, FILEPATH),
								     fourthNext = xml.read(ConsumerTag.fourthNext, FILEPATH),
								    	 supervisorSig = xml.read(ConsumerTag.supervisorSig, FILEPATH),
								    	 supervisorName = xml.read(ConsumerTag.supervisorName, FILEPATH),
								    	 supervisorDate = xml.read(ConsumerTag.supervisorDate, FILEPATH),								    	 
								    	 alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH),								    	 
								    	 OK = xml.read(ConsumerTag.OK, FILEPATH);
	
	public static class ConsumerTag {
		
		  public static final String  nearmiss = "nearmiss",
				  vehicle = "vehicle",
				  empNameSecond = "empNameSecond",
				  empName = "empName",
				  dateOfIncident = "dateOfIncident",
				  dateOfEvaluation = "dateOfEvaluation",
				  time = "time",
				  jobNumber = "jobNumber",
				  addressOfIncident = "addressOfIncident",
				  ExplainIncident = "ExplainIncident",
				  handTools = "handTools",
				  PowerTools = "PowerTools",
				  trainingYes = "trainingYes",
				  trainingText = "trainingText",
				  avoidYes = "avoidYes",
				  avoidText = "avoidText",
				  witnessYes = "witnessYes",
				  witnessText = "witnessText",
				  faulty = "faulty",
				  poor = "poor",
				  explainText = "explainText",
				  unit = "unit",
				  ticket = "ticket",
				  attachPhoto = "attachPhoto",
				  cameraRoll = "cameraRoll",
				  photoNameiOS11 = "photoNameiOS11",
				  photoNameiOS10 = "photoNameiOS10",
				  photoButton = "photoButton",
				  cameraButton = "cameraButton",
				  employeeSig = "employeeSig",
				  employeeName = "employeeName",
				  employeeDate = "employeeDate",
				  saveButton = "saveButton",
				  firstNext = "firstNext",
				  secondNext = "secondNext",
				  thirdNext = "thirdNext",
				  fourthNext = "fourthNext",
				  supervisorSig = "supervisorSig",
				  supervisorName = "supervisorName",
				  supervisorDate = "supervisorDate",
				  alertForSubmit = "alertForSubmit",
				  OK = "OK";				  
				                                      
	}
								    	 
	

}
