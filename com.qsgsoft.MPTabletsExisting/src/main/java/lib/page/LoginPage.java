package lib.page;


import org.openqa.selenium.WebDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import lib.locators.Login.Locators;
import qaframework.Configuration.Config_MobileAndWeb;
import org.openqa.selenium.support.ui.WebDriverWait;
import qaframework.WebElement.WebTimeConstant;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;



public class LoginPage {

	
	IOSDriver<MobileElement> driver;

	public LoginPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
	}

	public LoginPage enterPhoneNumber(String loginNo) throws Exception {
//		 WebDriverWait wait = new WebDriverWait(driver,
//				WebTimeConstant.WAIT_TIME_SMALL);
//
//		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.enterLoginNumber)));
		 driver.findElement(By.xpath(Locators.enterLoginNumber)).isDisplayed();
         driver.findElement(By.xpath(Locators.enterLoginNumber)).click();
         System.out.println("Logged In Number is" + loginNo);
         driver.findElement(By.xpath(Locators.enterLoginNumber)).sendKeys(loginNo);
		 return this;
	}
	
	public LoginPage clickSignIn() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.signIn)));
         driver.findElement(By.name(Locators.signIn)).click();
		 return this;
	}
	
	
	public LoginPage tap7times() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("IMG_1645.JPG")));
		 
		 MobileElement ele = driver.findElement(By.name("IMG_1645.JPG"));
//		 TouchAction  action = TouchAction(driver);
		 TouchAction  action = new TouchAction(driver).tap(ele).tap(ele).tap(ele).tap(ele).tap(ele).tap(ele).tap(ele).perform();

	
		 return this;
	}

	private TouchAction TouchAction(IOSDriver<MobileElement> driver2) {
		// TODO Auto-generated method stub
		return null;
	}


}
