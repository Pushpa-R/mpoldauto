package lib.page;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;

import lib.locators.CreateJobHazard.JobHazardLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;

public class JobHazardPage {

	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public JobHazardPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
    public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function select type of JobHazard in create page Date :
	 * 27-April-2017 Author : Ashwini
	 **********************************************************************************/
	public JobHazardPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();       
		 return this;
	}
	
//	/**********************************************************************************
//	 * Description : This function select type of JobHazard in create page Date :
//	 * 27-April-2017 Author : Ashwini
//	 **********************************************************************************/
//	public JobHazardPage tapElementByName(String locators) throws Exception {
//
//		WebDriverWait wait = new WebDriverWait(driver,
//				WebTimeConstant.WAIT_TIME_SMALL);
//
//		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
//		 System.out.println("locatorsValue" + locators);
//		 
//		 //WebElement element = driver.findElement(By.name(locators));
//         //driver.tap(2, element, (int) 0.8);         
//		 driver.findElement(By.name("Button")).click();
////        int bottomY = driver.findElement(By.name(locators)).getSize().getHeight(); 
////        int centerX = driver.findElement(By.name(locators)).getLocation().getX() + (driver.findElement(By.name(locators)).getSize().getWidth()/2); 
////         driver.tap(2, centerX, bottomY-80, 6); 
//         
//		 return this;
//	}
	
	/**********************************************************************************
	 * Description : This function select type of Fuel in create page
	 *  Date :25-April-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();         
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 27-April-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage enterValueInJobHazard(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 27-April-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage enterValueInJobHazardFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :27-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage takePicture() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(JobHazardLocators.cameraBtn)));
        driver.findElement(By.xpath(JobHazardLocators.cameraBtn)).click();             
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
        return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :24-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(JobHazardLocators.saveButton)));
        driver.findElement(By.name(JobHazardLocators.saveButton)).click();
		return this;
	}
	
//	/**********************************************************************************
//	 * Description : This function taps on next button
//	 * Date : 30-April-2017
//	 * Author : Ashwini Acharya
//	 **********************************************************************************/
//	public JobHazardPage clickNext(String locator) throws Exception {
//
//		WebDriverWait wait = new WebDriverWait(driver,
//				WebTimeConstant.WAIT_TIME_SMALL);
//
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
//        driver.findElement(By.name(locator)).click();
//		return this;
//
//	  }	
	
	/**********************************************************************************
	 * Description : This function taps on Photo button
	 * Date : 30-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(JobHazardLocators.photoBtn)));
        driver.findElement(By.xpath(JobHazardLocators.photoBtn)).click();        
        return this;
	}	
	
	/**********************************************************************************
	 * Description : This function taps on Photo button
	 * Date : 30-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage takePhotosNew() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		       
		 driver.findElement(By.name(JobHazardLocators.cameraRoll)).click();
	      if (driver.findElement(By.name(JobHazardLocators.photoNameiOS11)).isDisplayed()) {
	 	       driver.findElement(By.name(JobHazardLocators.photoNameiOS11)).click();
	      }else{
	 	       driver.findElement(By.name(JobHazardLocators.photoNameiOS10)).click();
	      }
	    driver.findElement(By.name("Use")).click();      
        return this;
	}	
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 24-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public JobHazardPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
		
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 22-Dec-2017 Author : Pushpa
	 **********************************************************************************/
	public JobHazardPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(JobHazardLocators.alertForSubmit)));
		driver.findElement(By.name(JobHazardLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(JobHazardLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public JobHazardPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(JobHazardLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(JobHazardLocators.OK)));
		    driver.findElement(By.name(JobHazardLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;	
	}
}
