package lib.page;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateDirectDeposit.DirectDepositLocators;
import lib.locators.CreateExpense.Locators;
import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import qaframework.custom.WaitForElement;

public class DirectDepositPage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public DirectDepositPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
    public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 11-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :11-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 11-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage enterValueInDirectDeposit(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 11-May-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage enterValueInDirectDepositFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage takePicture() throws Exception {	
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DirectDepositLocators.cameraButton)));
        driver.findElement(By.name(DirectDepositLocators.cameraButton)).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}	
	
	/**********************************************************************************
	 * Description : This function take a picture using Gallery
	 * Date :11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/	
	public DirectDepositPage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DirectDepositLocators.photoButton)));
        driver.findElement(By.name(DirectDepositLocators.photoButton)).click();   
        driver.findElement(By.name(DirectDepositLocators.cameraRoll)).click();
	     if (driver.findElement(By.name(DirectDepositLocators.photoNameiOS11)).isDisplayed()) {
	 	       driver.findElement(By.name(DirectDepositLocators.photoNameiOS11)).click();
	      }else{
	 	       driver.findElement(By.name(DirectDepositLocators.photoNameiOS10)).click();
	        }	     
        return this;
	}	
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DirectDepositLocators.saveButton)));
        driver.findElement(By.name(DirectDepositLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 11-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;
	  }	
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 08-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DirectDepositLocators.alertForSubmittedReport)));
		driver.findElement(By.name(DirectDepositLocators.alertForSubmittedReport)).isDisplayed();
		String actual = driver.findElement(By.name(DirectDepositLocators.alertForSubmittedReport)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 11-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(DirectDepositLocators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(DirectDepositLocators.OK)));
		    driver.findElement(By.name(DirectDepositLocators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :11-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage enterSignature(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		  TouchAction Action = new TouchAction(driver);
		  Action.press(100,100).moveTo(100,100).release().perform();		    
		  return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public DirectDepositPage scrollTableView(String locator) throws Exception {

    	      driver.findElement(By.xpath(locator)).isDisplayed();    	     
    	      int height = driver.manage().window().getSize().height;
    	      int width = driver.manage().window().getSize().width;    	    
    	      driver.swipe(width, height / 2, 0, height / 2, 250);    	          	      
          return this;
	  }		

}
