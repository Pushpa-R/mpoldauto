package lib.page;

import static org.testng.Assert.assertEquals;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateExpense.Locators;
import lib.locators.CreateIncident.IncidentLocators;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;


public class ProjectSitePage {
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public ProjectSitePage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function used click on the element by the name
	 *  Date : 08-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :08-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Incident
	 *  Date : 08-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage enterValueInProjectSite(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage takePicture() throws Exception {	
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ProjectSiteLocators.cameraButton)));
        driver.findElement(By.xpath(ProjectSiteLocators.cameraButton)).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}		

	/**********************************************************************************
	 * Description : This function take a picture using Gallery
	 * Date :08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/	
	public ProjectSitePage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ProjectSiteLocators.photoButton)));
        driver.findElement(By.xpath(ProjectSiteLocators.photoButton)).click();         
        return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using Gallery
	 * Date :08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/	
	public ProjectSitePage takePhotosNew() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);		       
		 driver.findElement(By.name(ProjectSiteLocators.cameraRoll)).click();
	     if (driver.findElement(By.name(ProjectSiteLocators.photoNameiOS11)).isDisplayed()) {
	 	       driver.findElement(By.name(ProjectSiteLocators.photoNameiOS11)).click();
	      }else{
	 	       driver.findElement(By.name(ProjectSiteLocators.photoNameiOS10)).click();
	        }
	      driver.findElement(By.name("Use")).click();      
          return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(ProjectSiteLocators.saveButton)));
        driver.findElement(By.name(ProjectSiteLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
		
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 08-May-2018 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(ProjectSiteLocators.alertForSubmit)));
		driver.findElement(By.name(ProjectSiteLocators.alertForSubmit)).isDisplayed();
		String actual = driver.findElement(By.name(ProjectSiteLocators.alertForSubmit)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 08-May-2018
	 *  Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage okButtonTappedForAlertSubmission(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(Locators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.OK)));
		    driver.findElement(By.name(Locators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function used to click on element using xpath
	 *  Date :08-May-2018
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage enterSignature(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		  TouchAction Action = new TouchAction(driver);
		  Action.press(100,100).moveTo(100,100).release().perform();		    
		  return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 08-May-2018
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public ProjectSitePage scrollTableView(String locator) throws Exception {

    	      driver.findElement(By.xpath(locator)).isDisplayed();    	     
    	      int height = driver.manage().window().getSize().height;
    	      int width = driver.manage().window().getSize().width;    	    
    	      driver.swipe(width, height / 2, 0, height / 2, 250);    	          	      
          return this;
	  }
}
