package lib.locators;

public class CreateWeeklyForeman {
	public static class WeeklyForemanLocators {
		public static final String timesheetIcn = "timesheet-512",
				weeklySegment = "Weekly",
				addButton = "plus 1",
				startDate = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]",
				endDate = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[2]",
				cancel = "Cancel",
				generate = "Generate",
				crewMembers = "Crew Members",
				equipment = "Equipment",
				doneButton = "Done",						
		        submitButton = "submit",
		        alertForSubmit = "Your timesheet is submitted to the Admin",
		        OK = "OK",
		        	doneSubmit = "Done",     
				createWeekly = "Generate from Dailies";
		
	}

}
