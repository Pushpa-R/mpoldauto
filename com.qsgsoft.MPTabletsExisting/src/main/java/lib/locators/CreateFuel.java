package lib.locators;

public class CreateFuel {	
	public static class FuelLocators {
		public static final String  fuelIcon = "FuelBtn",
		                                             addIcon = "plus 1",
				                                    jobNumber = "JobNumberTF",
				                                    receiptDate = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[2]",
				                                    vehicleNumber = "VehicleNumberTF",
				                                    mileage = "Mileage",
				                                    offLoadGallon = "OffRoadGallon",
				                                    remarks = "RemarksTW",
				                                    photoButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]",
				                                    cameraButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]",
				                                    saveButton = "Save",
				                                    submit = "Submit",
				                                    alertForSubmittedReport = "Your Fuel Receipt Report is Submitted",	
				                                    alertforPhotos = "You have already attached one Invoice. The new Invoice will overwrite the previous invoice attached. ",
		                                            OK ="OK",		
		                                             Allow = "Allow",
		                                            cameraRoll = "Moments",		                                          
		                                            photoNameiOS11 = "Photo, Portrait, 11 June, 11:47 AM",
                                                    photoNameiOS10 =	"Photo, Portrait, 11 June, 11:47 AM";
		                                            
	
	}

}
