package lib.locators;

public class CreateAtvSafety {
	public static class AtvSafetyLocators {
		public static final String AtvSafetyIcn = "atvSafetyIcn",
				                                addButton = "plus 1",
				                                downloadButton = "downloadImg",
				                                scrollView = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]",
				                                employeeName = "empName",
				                                employeeSignature = "empSig",
				                                employeeDate = "empDate",
				                                alertForSubmittedReport = "Your ATV Safety Procedure Report is Submitted",
				                                OK = "OK",
				                                done = "Done",
				                                submitButton = "Submit";			                                
		}
}
