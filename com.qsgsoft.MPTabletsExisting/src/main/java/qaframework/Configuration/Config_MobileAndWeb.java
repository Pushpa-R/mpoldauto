package qaframework.Configuration;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import qaframework.lib.UserDefinedFunction.Date_Time_Settings;
import qaframework.lib.UserDefinedFunction.OfficeCommonFunction;
import qaframework.lib.UserDefinedFunction.PathProperties;
//import qaframework.lib.UserDefinedFunctions.QnetFunctions;
import qaframework.WebElement.WebTimeConstant;

public class Config_MobileAndWeb {

	IOSDriver<MobileElement> driver;
	public static WebDriver wdriver = null;
	public static WebDriverWait waitVar = null;

    public IOSDriver<MobileElement> IOS_Driver;
	@SuppressWarnings("rawtypes")
	public static Class className;
	public static String strReason, gstrdate, gstrBuild, gstrTimetake,
			gstrFailureReason, TCID, strTO, strResultPath, strSessionId = "";
	public static Date gdtStartDate;
	public String gstrResult = "";
	protected String gstrReason;
	protected double gdbTimeTaken;
	public static long gslsysDateTime;

	PathProperties objPathProperties = new PathProperties();
	OfficeCommonFunction objOfficeFunctions = new OfficeCommonFunction();
	
	@BeforeMethod
	public void setResultToFalse() throws Exception{
		gstrResult = "FAIL";
		Date_Time_Settings dts = new Date_Time_Settings();
		gstrTimetake = dts.timeNow("HH:mm:ss");
		gstrdate = dts.getCurrentDate("yyyy-MM-dd");
		gdtStartDate = new Date();
	}

	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {

		DesiredCapabilities cap = new DesiredCapabilities();
		  ChromeOptions options = new ChromeOptions();
			System.getProperty("webdriver.chrome.wdriver", "/usr/local/bin/chromedriver.exe");
			cap.setBrowserName("Chrome");
		  cap.setCapability(ChromeOptions.CAPABILITY, options);
		  cap.setBrowserName("Chrome");
		  wdriver = new ChromeDriver(cap);
		  wdriver.manage().deleteAllCookies();
		  wdriver.manage().timeouts()
		    .implicitlyWait(WebTimeConstant.WAIT_TIME, TimeUnit.SECONDS);
		  wdriver.manage()
		    .timeouts()
		    .pageLoadTimeout(WebTimeConstant.WAIT_TIME_TOO_LONG,
		      TimeUnit.SECONDS);
		  wdriver.manage().window().maximize();
		

		// Launch mobile app
		  DesiredCapabilities capabilities = new DesiredCapabilities();
		 //capabilities.setCapability("appium-version", "1.8.0-beta3");
//	     capabilities.setCapability("platformName", "iOS");
//	     capabilities.setCapability("platformVersion", "11.1");
//	     capabilities.setCapability("deviceName", "iPad Air");
//	     capabilities.setCapability("bundleId", "com.MPTablets");
//	    capabilities.setCapability("app", "/Users/qsgtech/Downloads/MPTabletsNew.app");
//	     //capabilities.setCapability("app", "/Users/rishiristha/Documents/MPApps/MPTabletsNew.app");
//	     capabilities.setCapability("xcodeOrgId", "53DX276YWF");
//	     capabilities.setCapability("xcodeSigningId", "iPhone Developer");
//	     capabilities.setCapability("showXcodeLog", "true");
//	     capabilities.setCapability("useNewWDA", "true");
//	     capabilities.setCapability("automationName", "XCUITest");
//	     capabilities.setCapability("newCommandTimeout", "9000");
//		
//
//		capabilities.setCapability("noReset", "true");
		
		
		
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("platformVersion", "10.3.3");
		//capabilities.setCapability("platformVersion", "9.3.3");
		//capabilities.setCapability("platformVersion", "10.3.3");
		//capabilities.setCapability("deviceName", "QSG's iPad");
		capabilities.setCapability("deviceName", "QSG-test4ignore");
		//capabilities.setCapability("deviceName", "QSG-test3ignore");
		//capabilities.setCapability("deviceName", "Administrator’s iPad");
		
		//capabilities.setCapability("udid", "1d85d337f9a5f7ad78be9aa6b8d9ad137f40545d");
		capabilities.setCapability("udid", "1d85d337f9a5f7ad78be9aa6b8d9ad137f40545d");
		//capabilities.setCapability("udid", "533cb9aa4f53070cb71c62ea7db08e8a1a160996");
		//capabilities.setCapability("udid", "b074608aa35930c5a2df335f08a815dfca31027b");
		capabilities.setCapability("browser","safari");
		capabilities.setCapability("bundleId", "com.QSG.MPTablets");
		//capabilities.setCapability("app", "/Users/qsgtech/Desktop/MP_OldApp_ipaFile/MPTablets.ipa");
		//capabilities.setCapability("app", "/Users/qsgtech/Documents/MpAutomationIpa/MPTabletsNewAuto.ipa");
		capabilities.setCapability("app", "/Users/qsgtech/Desktop/MP5.7.11IPA/MPTablets (14).ipa");
		capabilities.setCapability("bootstrapPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent");
		capabilities.setCapability("agentPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj");
		capabilities.setCapability("showXcodeLog", "true");
		capabilities.setCapability("automationName", "XCUITest");
		capabilities.setCapability("noReset", false);
        capabilities.setCapability("newCommandTimeout", 120);
        capabilities.setCapability ("autoAcceptAlerts",true);
        capabilities.setCapability("xcodeOrgId", "seema nair");
		capabilities.setCapability("xcodeSigningId", "iPhone Developer");

		  //for simulator
//		  capabilities.setCapability("platformName", "iOS");
//			capabilities.setCapability("platformVersion", "11.2");
//			capabilities.setCapability("deviceName", "iPad Air 2");
//			capabilities.setCapability("browser","safari");
//			capabilities.setCapability("bundleId", "io.appium.WebDriverAgentRunner.TestTap");
//			capabilities.setCapability("app", "/Users/qsgtech/Downloads/TestTap.app");
//			capabilities.setCapability("bootstrapPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent");
//			capabilities.setCapability("agentPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj");
//			capabilities.setCapability("showXcodeLog", "true");
//			capabilities.setCapability("automationName", "XCUITest");
//			capabilities.setCapability("noReset", false);
//	        capabilities.setCapability("newCommandTimeout", 120);
//	        capabilities.setCapability ("autoAcceptAlerts",true);
//	        


		capabilities.setCapability("noReset", "true");
		
		
		IOS_Driver = new IOSDriver<MobileElement>(new URL(
				"http://127.0.0.1:4723/wd/hub"), capabilities);
		IOS_Driver
				.manage()
				.timeouts()
				.implicitlyWait(WebTimeConstant.WAIT_TIME_LONG,
						TimeUnit.SECONDS);

	}
	
	@AfterMethod
	public void writtingResults() throws Exception{
		Properties pathProps = objPathProperties.Read_FilePath();
		//QnetFunctions objQnet = new QnetFunctions();
		gdbTimeTaken = objOfficeFunctions.TimeTaken(gdtStartDate);
		gstrBuild = pathProps.getProperty("Build");
		// Write the test result to excel.
		String Resultpath = pathProps.getProperty("ResultsPath");
		strResultPath = System.getProperty("user.dir") + Resultpath;

		//objQnet.WriteResultdb_Excel(TCID, strTO, gstrResult, gdbTimeTaken,
				//strResultPath);
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {		
		try {
			   if (wdriver != null) {
			    wdriver.close();
			    wdriver.quit();
			    wdriver = null;
			   }
			   if (IOS_Driver != null) {
				   IOS_Driver.quit();
			   }
			  } catch (Exception e) {
			   e.printStackTrace();
			  }
	}

}
