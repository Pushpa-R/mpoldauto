package qaframework.lib.UserDefinedFunction;

import java.io.InputStream;
import java.util.Properties;
public class PathProperties {

	/****************************************
	 * Function to read the excel path
	 *****************************************/
	public Properties Read_FilePath() throws Exception {
		/*Properties autoitProps = new Properties();
		URL url = getClass().getResource("/path.properties");
		InputStream is = url.openStream();
		autoitProps.load(is);
		return autoitProps;*/
		
		Properties autoitProps = new Properties();
		InputStream is = this.getClass().getResourceAsStream(
				System.getProperty("path.file",
						"/PropertiesFiles/path.properties"));
		autoitProps.load(is);
		return autoitProps;
	}
}
