package requirementValidation.DirectDeposit;

import lib.dataObject.LoginData;
import org.testng.annotations.Test;
import lib.dataObject.DirectDepositData;
import lib.locators.CreateDirectDeposit.DirectDepositLocators;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import qaframework.Configuration.Config_MobileAndWeb;
import lib.page.DirectDepositPage;

public class DirectDepositValidation extends Config_MobileAndWeb {

	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		DirectDepositPage directDeposit = new DirectDepositPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickDirectDeposit();
		directDeposit.clickElementByName(DirectDepositLocators.addIcon);		
	}
	
	 public void createDirectDeposit() throws Exception {			
		    DirectDepositPage directDeposit = new DirectDepositPage(IOS_Driver);
		    DirectDepositData objDirectDeposit = new DirectDepositData();	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.employeeName, DirectDepositLocators.employeeName);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.ssn, DirectDepositLocators.ssn);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.address, DirectDepositLocators.address);
		    directDeposit.clickElementByName(DirectDepositLocators.state);	
		    directDeposit.clickElementByxpath(DirectDepositLocators.stateCell);	
		    directDeposit.clickElementByName(DirectDepositLocators.doneButton);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.city, DirectDepositLocators.city);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.zip, DirectDepositLocators.zip);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.phone, DirectDepositLocators.phone);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.bankName, DirectDepositLocators.bankName);	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.accountName, DirectDepositLocators.accountName);	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.route, DirectDepositLocators.route);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.phoneNumber, DirectDepositLocators.phoneNumber);
		    directDeposit.enterValueInDirectDepositFromxpath(objDirectDeposit.bankAddress, DirectDepositLocators.bankAddress);
		    directDeposit.scrollTableView(DirectDepositLocators.scrollView);
		    directDeposit.enterSignature(DirectDepositLocators.supervisorSig);
		    directDeposit.clickElementByName(DirectDepositLocators.tickButton);	
		    directDeposit.clickElementByName(DirectDepositLocators.attachButton);	
		    directDeposit.takePhotos();	 
		 	//directDeposit.okButtonTappedForAlertSubmission(objDirectDeposit.OK);
		 	//directDeposit.takePhotosNew(); 	 
		 	directDeposit.clickElementByName(DirectDepositLocators.saveButton);				    
		}
	 
	 public void createDirectDepositSecond() throws Exception {			
		    DirectDepositPage directDeposit = new DirectDepositPage(IOS_Driver);
		    DirectDepositData objDirectDeposit = new DirectDepositData();	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.emplyeeNameSecond, DirectDepositLocators.employeeName);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.ssn, DirectDepositLocators.ssn);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.address, DirectDepositLocators.address);
		    directDeposit.clickElementByName(DirectDepositLocators.state);	
		    directDeposit.clickElementByxpath(DirectDepositLocators.stateCell);	
		    directDeposit.clickElementByName(DirectDepositLocators.doneButton);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.city, DirectDepositLocators.city);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.zip, DirectDepositLocators.zip);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.phone, DirectDepositLocators.phone);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.bankName, DirectDepositLocators.bankName);	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.accountName, DirectDepositLocators.accountName);	
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.route, DirectDepositLocators.route);
		    directDeposit.enterValueInDirectDeposit(objDirectDeposit.phoneNumber, DirectDepositLocators.phoneNumber);
		    directDeposit.enterValueInDirectDepositFromxpath(objDirectDeposit.bankAddress, DirectDepositLocators.bankAddress);
		    directDeposit.scrollTableView(DirectDepositLocators.scrollView);
		    directDeposit.enterSignature(DirectDepositLocators.supervisorSig);
		    directDeposit.clickElementByName(DirectDepositLocators.tickButton);	
		    directDeposit.clickElementByName(DirectDepositLocators.attachButton);	
		    //directDeposit.takePhotos();	 
		 	//directDeposit.okButtonTappedForAlertSubmission(objDirectDeposit.OK);
		 	//directDeposit.takePhotosNew(); 	 
		    directDeposit.takePicture();
		 	directDeposit.clickElementByName(DirectDepositLocators.saveButton);				    
		}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 11-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/		
		@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {
			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			DirectDepositPage directDeposit = new DirectDepositPage(IOS_Driver);
			DirectDepositData objdirectDepositData = new DirectDepositData();
			login();
			createDirectDeposit();			
			directDeposit.clickSubmit(DirectDepositLocators.submitButton);	
			directDeposit.verReportIsSubmitted(objdirectDepositData.alertForSubmittedReport);
			directDeposit.okButtonTappedForAlertSubmission(objdirectDepositData.OK);		
		}
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 11-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {
			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			DirectDepositPage directDeposit = new DirectDepositPage(IOS_Driver);
			DirectDepositData objdirectDepositData = new DirectDepositData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			directDeposit.clickElementByName(DirectDepositLocators.addIcon);		
			createDirectDepositSecond();			
			directDeposit.clickSubmit(DirectDepositLocators.submitButton);		
			directDeposit.verReportIsSubmitted(objdirectDepositData.alertForSubmittedReport);
			directDeposit.okButtonTappedForAlertSubmission(objdirectDepositData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}
	
}
