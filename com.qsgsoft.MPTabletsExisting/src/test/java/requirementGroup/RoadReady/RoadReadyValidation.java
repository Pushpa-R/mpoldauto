package requirementGroup.RoadReady;

import org.testng.annotations.Test;

import lib.dataObject.RoadReadyData;
import lib.dataObject.IncidentData;
import lib.dataObject.LoginData;
import lib.page.RoadReadyPage;
import lib.page.HomePage;
import lib.page.IncidentPage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateRoadReady;
import lib.locators.CreateRoadReady.RoadReadyLocators;
import lib.locators.Yopmail;
import lib.locators.CreateIncident.IncidentLocators;
import qaframework.Configuration.Config_MobileAndWeb;

public class RoadReadyValidation extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		RoadReadyPage roadReady = new RoadReadyPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.MechanicLoggedInNumber).clickSignIn();
		objHome.clickRoadReady();
		roadReady.clickElementByName(RoadReadyLocators.addIcn);		
	}
	
	 public void createRoadReady() throws Exception {
			
		    RoadReadyPage roadReady = new RoadReadyPage(IOS_Driver);
		    RoadReadyData objRoadReady = new RoadReadyData();			
		    roadReady.enterValueInRoadReady(objRoadReady.unit, RoadReadyLocators.unit);
		    roadReady.enterValueInRoadReady(objRoadReady.trailer, RoadReadyLocators.trailer);
		    roadReady.enterValueInRoadReady(objRoadReady.fromJob, RoadReadyLocators.fromJob);
		    roadReady.enterValueInRoadReady(objRoadReady.toJob, RoadReadyLocators.toJob);		
		    roadReady.clickElementByName(RoadReadyLocators.firstCellNa);
		    roadReady.clickElementByxpath(RoadReadyLocators.secondCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.thirdCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.fourthCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.fifthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.sixthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.seventhCellNa);		    
		    roadReady.clickElementByxpath(RoadReadyLocators.eightCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.ninthCellVer);	
		    roadReady.clickElementByxpath(RoadReadyLocators.fireExtinguisher);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.tenthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.eleventhCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twelthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.thirteenCellNa);		    
		    roadReady.clickElementByxpath(RoadReadyLocators.fourteenCellNa);	
		    roadReady.scrollTableView(RoadReadyLocators.cellScroll);	   
		    roadReady.scrollTableView(RoadReadyLocators.tableViewScroll);			   
		    roadReady.clickElementByxpath(RoadReadyLocators.fifteenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.sixteenCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.seventeenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.eighteenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.nineteenCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.twenteethCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyfirstCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentySecondCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyThirdCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyFourthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyFifthCellNa);			  
		    roadReady.enterValueInRoadReady(objRoadReady.employeeName, RoadReadyLocators.employeeName);		    				
		    roadReady.enterSignature(RoadReadyLocators.empSignature);		
		    roadReady.clickSave();				
	}
	 
	 public void createRoadReadySecond() throws Exception {
			
		    RoadReadyPage roadReady = new RoadReadyPage(IOS_Driver);
		    RoadReadyData objRoadReady = new RoadReadyData();			
		    roadReady.enterValueInRoadReady(objRoadReady.unitsecond, RoadReadyLocators.unit);
		    roadReady.enterValueInRoadReady(objRoadReady.trailer, RoadReadyLocators.trailer);
		    roadReady.enterValueInRoadReady(objRoadReady.fromJob, RoadReadyLocators.fromJob);
		    roadReady.enterValueInRoadReady(objRoadReady.toJob, RoadReadyLocators.toJob);		
		    roadReady.clickElementByName(RoadReadyLocators.firstCellNa);
		    roadReady.clickElementByxpath(RoadReadyLocators.secondCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.thirdCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.fourthCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.fifthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.sixthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.seventhCellNa);		    
		    roadReady.clickElementByxpath(RoadReadyLocators.eightCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.ninthCellVer);	
		    roadReady.clickElementByxpath(RoadReadyLocators.fireExtinguisher);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.tenthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.eleventhCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twelthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.thirteenCellNa);		    
		    roadReady.clickElementByxpath(RoadReadyLocators.fourteenCellNa);	
		    roadReady.scrollTableView(RoadReadyLocators.cellScroll);	   
		    roadReady.scrollTableView(RoadReadyLocators.tableViewScroll);			   
		    roadReady.clickElementByxpath(RoadReadyLocators.fifteenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.sixteenCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.seventeenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.eighteenCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.nineteenCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.twenteethCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyfirstCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentySecondCellNa);			    
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyThirdCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyFourthCellNa);	
		    roadReady.clickElementByxpath(RoadReadyLocators.twentyFifthCellNa);			  
		    roadReady.enterValueInRoadReady(objRoadReady.employeeName, RoadReadyLocators.employeeName);		    				
		    roadReady.enterSignature(RoadReadyLocators.empSignature);		
		    roadReady.clickSave();				
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 07-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			RoadReadyPage roadReady = new RoadReadyPage(IOS_Driver);
			RoadReadyData objroadReadyData = new RoadReadyData();
			login();
			createRoadReady();			
			roadReady.clickSubmit(RoadReadyLocators.submitButton);			
			roadReady.verReportIsSubmitted(RoadReadyLocators.alertForSubmit);
			roadReady.okButtonTappedForAlertSubmission(RoadReadyLocators.OK);		
		}
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 07-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {

			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			RoadReadyPage roadReady = new RoadReadyPage(IOS_Driver);
			RoadReadyData objroadReadyData = new RoadReadyData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			roadReady.clickElementByName(RoadReadyLocators.addIcn);		
			createRoadReadySecond();			
			roadReady.clickSubmit(RoadReadyLocators.submitButton);		
			roadReady.verReportIsSubmitted(objroadReadyData.alertForSubmit);
			roadReady.okButtonTappedForAlertSubmission(objroadReadyData.OK);		   
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}

}
