package requirementGroup.JobHazard;

import org.testng.annotations.Test;

import lib.dataObject.FuelData;
import lib.dataObject.JobHazardData;
import lib.dataObject.LoginData;
import lib.page.JobHazardPage;
import lib.page.FuelPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateJobHazard;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.Yopmail;
import lib.locators.CreateFuel.FuelLocators;
import qaframework.Configuration.Config_MobileAndWeb;

public class JobHazardVallidation extends Config_MobileAndWeb {
	
	public void login() throws Exception  {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickJobHazard();
		jobHazard.clickElementByName(JobHazardLocators.addIcon);		
	}
	
	 public void createJobHazard() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumber, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 					
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazardFor1stScript() throws Exception {	
		 
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();		
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumber, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);									
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);				
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);		
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);	
	}
	 
	 public void createJobHazard2ndScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();		
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumbersecond, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationSecond, JobHazardLocators.projectLocation);	
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);			
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);								
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);	
	}
	 
	 public void createJobHazard3rdScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();		
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberThird, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationThird, JobHazardLocators.projectLocation);				    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);		
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);		
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 					
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);		
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);		
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazard4thScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();		
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberFourth, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationFourth, JobHazardLocators.projectLocation);
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);				
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			//jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			//jobHazard.takePhotosNew(); 	
			jobHazard.takePicture();
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazard6thScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberSixth, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationSixth, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo6, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description6, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName6, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber6, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes6, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName6, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber6, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes6, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName6, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber6, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes6, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation6, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit6,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage6, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser6, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder6, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman6, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman6, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice6, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator6, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer6, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other6, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions6, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 		
			jobHazard.takePhotos();					
		    jobHazard.takePhotosNew(); 	
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);	
	}
	 
	 public void createJobHazard7thScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberSeventh, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationSeventh, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo7, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description7, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName7, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber7, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes7, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName7, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber7, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes7, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName7, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber7, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes7, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		    
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation7, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit7,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage7, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser7, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder7, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman7, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman7, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice7, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator7, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer7, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other7, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions7, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 		
			jobHazard.takePhotos();					
		    jobHazard.takePhotosNew(); 	
		    jobHazard.takePhotos();					
		    jobHazard.takePhotosNew();
		    jobHazard.takePhotos();					
		    jobHazard.takePhotosNew();
		    jobHazard.takePhotos();					
		    jobHazard.takePhotosNew();
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazard8thScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberEight, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationEighth, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo8, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description8, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName8, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber8, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes8, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName8, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber8, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes8, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName8, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber8, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes8, JobHazardLocators.nearestEmsNotes);		
		    	jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation8, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit8,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage8, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser8, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder8, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman8, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman8, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice8, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator8, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer8, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other8, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions8, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 	
			jobHazard.takePhotos();				
			jobHazard.takePhotosNew(); 
			jobHazard.takePhotos();						
			jobHazard.takePhotosNew();
			jobHazard.takePhotos();						
			jobHazard.takePhotosNew(); 
			jobHazard.takePhotos();						
			jobHazard.takePhotosNew(); 
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazard9thScript() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumberNine, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocationNineth, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo9, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description9, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName9, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber9, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes9, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName9, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber9, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes9, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName9, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber9, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes9, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation9, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit9,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage9, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser9, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder9, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman9, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman9, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice9, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator9, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer9, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other9, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions9, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();						
			//jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 					
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);			
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazardSecond() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumbersecond, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);		   
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();	 
			jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 	
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);					       
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 30-4-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
//		@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
//	    public void RTS1() throws Exception {
//
//			TCID = "RTS1";
//			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
//			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
//			JobHazardData objjobHazardData = new JobHazardData();
//			login();
//			createJobHazard();			
//			jobHazard.clickSubmit(JobHazardLocators.submitButton);
//			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
//			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);		
//		}
		
		@Test(priority = 1, description = "Create and Submit the report with only Mandatory Fields")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Create and Submit the report with only Mandatory Fields";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			login();
			createJobHazardFor1stScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 2, description = "Create and Submit the report with only Mandatory Fields, Voice Record and No Attachments")
	    public void RTS2() throws Exception {

			TCID = "RTS2";
			strTO = "Create and Submit the report with only Mandatory Fields, Voice Record and No Attachments";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard2ndScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 3, description = "Create and Submit the report with only Mandatory Fields, Attachments and No Voice Record")
	    public void RTS3() throws Exception {

			TCID = "RTS3";
			strTO = "Create and Submit the report with only Mandatory Fields, Attachments and No Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard3rdScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 4, description = "Create and Submit the report with only Mandatory Fields, Attachments and No Voice Record")
	    public void RTS4() throws Exception {

			TCID = "RTS4";
			strTO = "Create and Submit the report with only Mandatory Fields, Attachments and No Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard4thScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 5, description = "Create and Submit the report with All Fields, Attachments and Voice Record")
	    public void RTS5() throws Exception {

			TCID = "RTS5";
			strTO = "Create and Submit the report with All Fields, Attachments and Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 6, description = "Create and Submit the report with All Fields,2 Attachments and Voice Record")
	    public void RTS6() throws Exception {

			TCID = "RTS6";
			strTO = "Create and Submit the report with All Fields, 2 Attachments and Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard6thScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 7, description = "Create and Submit the report with All Fields,5 Attachments and Voice Record")
	    public void RTS7() throws Exception {

			TCID = "RTS7";
			strTO = "Create and Submit the report with All Fields, 5 Attachments and Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			//login();
			createJobHazard7thScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 8, description = "Create and Submit the report with All Fields,5 Attachments and No Voice Record")
	    public void RTS8() throws Exception {

			TCID = "RTS8";
			strTO = "Create and Submit the report with All Fields, 5 Attachments and No Voice Record";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			//login();
			createJobHazard8thScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
		
		@Test(priority = 9, description = "Create and Submit the report with All Fields, Attachments, start and stop voice record in first page")
	    public void RTS9() throws Exception {

			TCID = "RTS3";
			strTO = "Create and Submit the report with All Fields, Attachments, start and stop voice record in first page";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);
			createJobHazard9thScript();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);	
			objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
			gstrResult = "PASS";
		}
	 
//		/**********************************************************************************************************
//		 * 'Description: Verify that able to create the report with attachement and submit
//		 *  'Date: 02-May-2018
//		 * 'Author: Ashwini Acharya
//		 **********************************************************************************************************/
		
//		@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
//	    public void RTS2() throws Exception {
//
//			TCID = "RTS2";
//			strTO = "Verify that able to create the report with attachement and submit and receive the email";
//			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
//			JobHazardData objjobHazardData = new JobHazardData();
//			YopMailPage objYopMail = new YopMailPage(this.wdriver);
//			jobHazard.clickElementByName(JobHazardLocators.addIcon);		
//			createJobHazardSecond();			
//			jobHazard.clickSubmit(JobHazardLocators.submitButton);
//			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
//			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);
//		    // launch yopmail
//		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
//		    gstrResult = "PASS";
//			
//		}
   
}
