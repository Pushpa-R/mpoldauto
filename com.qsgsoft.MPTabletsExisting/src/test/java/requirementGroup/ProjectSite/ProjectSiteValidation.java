package requirementGroup.ProjectSite;

import org.testng.annotations.Test;

import lib.dataObject.ProjectSiteData;
import lib.dataObject.IncidentData;
import lib.dataObject.LoginData;
import lib.page.ProjectSitePage;
import lib.page.HomePage;
import lib.page.IncidentPage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateProjectSite;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import lib.locators.Yopmail;
import qaframework.Configuration.Config_MobileAndWeb;

public class ProjectSiteValidation extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		ProjectSitePage projectSite = new ProjectSitePage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickProjectSite();
		projectSite.clickElementByName(ProjectSiteLocators.addIcon);		
	}
	
	 public void createProjectSite() throws Exception {
			
		    ProjectSitePage projectSite = new ProjectSitePage(IOS_Driver);
		    ProjectSiteData objProjectSite = new ProjectSiteData();		    
		    projectSite.clickElementByName(ProjectSiteLocators.date);	
		    projectSite.enterValueInProjectSite(objProjectSite.foreman, ProjectSiteLocators.foreman);
		    projectSite.enterValueInProjectSite(objProjectSite.jobNumber, ProjectSiteLocators.jobNumber);
		    projectSite.enterValueInProjectSite(objProjectSite.crewLocation, ProjectSiteLocators.crewLocation);
		    projectSite.enterValueInProjectSite(objProjectSite.siteInspection, ProjectSiteLocators.siteInspection);		    
		    projectSite.clickElementByName(ProjectSiteLocators.corrNeededYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.correctionText, ProjectSiteLocators.correctionText);			   
		    projectSite.enterValueInProjectSite(objProjectSite.workPracticeText, ProjectSiteLocators.workPracticeText);	
		    projectSite.clickElementByName(ProjectSiteLocators.unsafeWorkNo);	
		    projectSite.enterValueInProjectSite(objProjectSite.unsafeWorkText, ProjectSiteLocators.unsafeWorkText);	
		    projectSite.scrollTableView(ProjectSiteLocators.scrollView);
		    projectSite.clickElementByName(ProjectSiteLocators.toolsYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.toolsText, ProjectSiteLocators.toolsText);
		    projectSite.clickElementByName(ProjectSiteLocators.ppeYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.ppeText, ProjectSiteLocators.ppeText);			    
		    	projectSite.clickElementByName(ProjectSiteLocators.attachButton);
		    	projectSite.takePhotos();	 
		    //	projectSite.okButtonTappedForAlertSubmission(objProjectSite.OK);
		    	projectSite.takePhotosNew(); 	    
		    projectSite.enterSignature(ProjectSiteLocators.inspectorSign);
		    projectSite.enterValueInProjectSite(objProjectSite.inspectedName, ProjectSiteLocators.inspectedName);
			projectSite.clickElementByName(ProjectSiteLocators.saveButton);				
	}
	 
	 public void createProjectSiteSecond() throws Exception {
			
		    ProjectSitePage projectSite = new ProjectSitePage(IOS_Driver);
		    ProjectSiteData objProjectSite = new ProjectSiteData();		    
		    projectSite.clickElementByName(ProjectSiteLocators.date);	
		    projectSite.enterValueInProjectSite(objProjectSite.foreman, ProjectSiteLocators.foreman);
		    projectSite.enterValueInProjectSite(objProjectSite.jobNumberSecond, ProjectSiteLocators.jobNumber);
		    projectSite.enterValueInProjectSite(objProjectSite.crewLocation, ProjectSiteLocators.crewLocation);
		    projectSite.enterValueInProjectSite(objProjectSite.siteInspection, ProjectSiteLocators.siteInspection);		    
		    projectSite.clickElementByName(ProjectSiteLocators.corrNeededYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.correctionText, ProjectSiteLocators.correctionText);			   
		    projectSite.enterValueInProjectSite(objProjectSite.workPracticeText, ProjectSiteLocators.workPracticeText);	
		    projectSite.clickElementByName(ProjectSiteLocators.unsafeWorkNo);	
		    projectSite.enterValueInProjectSite(objProjectSite.unsafeWorkText, ProjectSiteLocators.unsafeWorkText);	
		    projectSite.scrollTableView(ProjectSiteLocators.scrollView);
		    projectSite.clickElementByName(ProjectSiteLocators.toolsYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.toolsText, ProjectSiteLocators.toolsText);
		    projectSite.clickElementByName(ProjectSiteLocators.ppeYes);	
		    projectSite.enterValueInProjectSite(objProjectSite.ppeText, ProjectSiteLocators.ppeText);			    
		    	projectSite.clickElementByName(ProjectSiteLocators.attachButton);
		    //	projectSite.takePhotos();	 
		    //	projectSite.okButtonTappedForAlertSubmission(objProjectSite.OK);
		   //	projectSite.takePhotosNew(); 	    
		    	projectSite.takePicture();
		    projectSite.enterSignature(ProjectSiteLocators.inspectorSign);
		    projectSite.enterValueInProjectSite(objProjectSite.inspectedName, ProjectSiteLocators.inspectedName);
			projectSite.clickElementByName(ProjectSiteLocators.saveButton);				
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 08-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			ProjectSitePage projectSite = new ProjectSitePage(IOS_Driver);
			ProjectSiteData objprojectSiteData = new ProjectSiteData();
			login();
			createProjectSite();			
			projectSite.clickSubmit(ProjectSiteLocators.submitButton);	
			projectSite.verReportIsSubmitted(objprojectSiteData.alertForSubmit);
			projectSite.okButtonTappedForAlertSubmission(objprojectSiteData.OK);		
		}
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 08-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {

			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			ProjectSitePage projectSite = new ProjectSitePage(IOS_Driver);
			ProjectSiteData objprojectSiteData = new ProjectSiteData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			projectSite.clickElementByName(ProjectSiteLocators.addIcon);		
			createProjectSiteSecond();			
			projectSite.clickSubmit(ProjectSiteLocators.submitButton);		
			projectSite.verReportIsSubmitted(objprojectSiteData.alertForSubmit);
			projectSite.okButtonTappedForAlertSubmission(objprojectSiteData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}

}
